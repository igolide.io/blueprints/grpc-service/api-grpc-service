import com.google.protobuf.gradle.builtins
import com.google.protobuf.gradle.generateProtoTasks
import com.google.protobuf.gradle.id
import com.google.protobuf.gradle.ofSourceSet
import com.google.protobuf.gradle.plugins
import com.google.protobuf.gradle.proto
import com.google.protobuf.gradle.protobuf
import com.google.protobuf.gradle.protoc

plugins {
    java
    id("com.google.protobuf") version "0.8.18"
    id("maven-publish")
}

group = "io.igolide.blueprints"
version = System.getenv("VERSION") ?: "0.0.0"
val mavenRegistry = "https://gitlab.com/api/v4/projects/36857540/packages/maven"

repositories {
    mavenCentral()
}

sourceSets {
    main {
        proto {
            srcDir("specification")
        }
    }
}

dependencies {
    implementation("javax.annotation:jsr250-api:1.0")
    implementation("com.google.protobuf:protobuf-java:3.21.0")
    implementation("io.grpc:grpc-all:1.47.0")
}

protobuf {
    protoc { artifact = "com.google.protobuf:protoc:3.21.1" }
    plugins {
        id("grpc") { artifact = "io.grpc:protoc-gen-grpc-java:1.47.0" }
    }
    generateProtoTasks {
        ofSourceSet("main").forEach { task ->
            task.builtins {
                getByName("java") {
                    outputSubDir = "java"
                }
            }
            task.plugins {
                id("grpc") {
                    outputSubDir = "java"
                }
            }
        }
    }
    generatedFilesBaseDir = "$projectDir/src/"
}

tasks.getByPath("clean").doLast {
    delete(protobuf.protobuf.generatedFilesBaseDir)
}

publishing {
    repositories {
        maven {
            url = uri(mavenRegistry)
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["java"])
        }
    }
}
